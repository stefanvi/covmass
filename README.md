# CoVMass Hardware Dokumentation


Welcome to the CovMass Documentation Git

If you would like to adapt the CoVMass project or are interested in the Hardware parts of the Project please feel free to explore this Git.

**What is CoVmass?**
The CoVMass project is an initiative of several departments of ETH Zurich. Its goal is to stop the spreading of the Sars-Cov-2 virus at ETH. This documentation contains the Hardware parts necessary to achieve such a system. 
If you would like to know more about the initiative please visit us on our website or read the document "Hardware documentation" to find additional links and Information: [CoVMass Official Website](https://covidtesting.ethz.ch/en/home-3/).

If you are interested in the development of the CoVMass Hardware please check out the [33-Hour-Hardware Hackathon Viodeo](https://www.youtube.com/watch?v=NMMvBLTGAqA&ab_channel=pdzzurich). 

**Dispenser System:**
CoVMass partnered with Selecta to develop a reliable PCR-Saliva-Test dispenser. If you are interested in the Dispenser system please check out the document "Hardware Documentation". It describes not only the project but also contains a manual for the Dispenser modification.

**Collector**
The manual to build your own PCR-Saliva-Test Collector system along with all prt. files and a manual can be found in the folder "Collector" 

**Publications and Additional Info**
We are currently working towards a first publication. The link will be supplied as soon as it is published.


